****************************************************************
**Algorithms & Data structures using C.**
****************************************************************

****************************************************************
Count : 137
****************************************************************

THIS PROJECT HAS BEEN MOVED TO [GitHub Account](https://github.com/msreekan/algorithms-ds)  

Bitbucket repository is kept around to just keep the old links alive.

****************************************************************
###[*Puzzles*]


+ [1. Container with most water](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Puzzles/11.ContainerWithMostWater/?at=master)

+ [2. Letter Combinations of a PhoneNumber](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Puzzles/17.LetterCombinationsofaPhoneNumber/?at=master)

+ [3. Trapping Rain Water](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Puzzles/42.TrappingRainWater/?at=master)

+ [4. Jump Game](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Puzzles/45.JumpGameII/?at=master)

+ [5. House Robber III](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Puzzles/337.HouseRobberIII/?at=master)

+ [6. Generate Parentheses](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Puzzles/22.GenerateParentheses/?at=master)

+ [7. Valid Sudoku](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/dadab6b5a5319aa9ce7acde7b7192edba1428056/Puzzles/36.ValidSudoku/?at=master)

+ [8. Sudoku Solver](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/d49de742e987695305b30e3ce2613b972e0746eb/Puzzles/37.SudokuSolver/?at=master)

+ [9. ZigZag Conversion](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/4836d621572bde813c96f2cbf43f3352e43dc4d5/Puzzles/6.ZigZagConversion/?at=master)

+ [10. N-Queens](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/880f22ac7391561c44056954835c60c95b06f09a/Puzzles/51.N-Queens/?at=master)

+ [11. N-QueensII](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/563bff04062df0baa8fc7b10ee3a5951912cfcf4/Puzzles/52.N-QueensII/?at=master)

+ [12. Jump Game](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/880f22ac7391561c44056954835c60c95b06f09a/Puzzles/55.JumpGame/?at=master)

+ [13. Best Time to Buy and Sell Stock](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/6325fa4cc92a8232c31f3593e87e64e36df53a31/Puzzles/121.BestTimetoBuyandSellStock/?at=master)

+ [14. Best Time to Buy and Sell Stock II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/6325fa4cc92a8232c31f3593e87e64e36df53a31/Puzzles/122.BestTimetoBuyandSellStockII/?at=master)

+ [15. Best Time to Buy and Sell Stock III](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/6325fa4cc92a8232c31f3593e87e64e36df53a31/Puzzles/123.BestTimetoBuyandSellStockIII/?at=master)

+ [16. Surrounded Regions](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/579cb511201452d62f07174eae885ebf67b58910/Puzzles/130.SurroundedRegions/?at=master)

+ [17. Gas Station](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/60363925d22b2a032333fa8e9f0b08b8ea1cb4f9/Puzzles/134.GasStation/?at=master)

+ [18. Candy](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Puzzles/135.Candy/?at=master)

+ [19. Single Number](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Puzzles/136.SingleNumber/?at=master)

+ [20. Single Number II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Puzzles/137.SingleNumberII/?at=master)


****************************************************************

****************************************************************
###[Dynamic Programming]

+ [21. Climbing Stairs](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Dynamic_Programming/70.ClimbingStairs/?at=master)

+ [22. Largest Rectangle in Histogram](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Dynamic_Programming/84.LargestRectangleinHistogram/?at=master)

+ [23. Decode Ways](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Dynamic_Programming/91.DecodeWays/?at=master)

+ [24. Substring with Concatenation of All Words](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/1aae5af96409f4f9267f21d2aa462d6f8db86d8f/Dynamic_Programming/30.SubstringwithConcatenationofAllWords/?at=master)

+ [25. Longest Valid Parentheses](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/e68cdd5d47d850e065d9ffacd1f34691f44963e3/Dynamic_Programming/32.LongestValidParentheses/?at=master)

+ [26. Regular Expression Matching](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/789a1282f1b09de5b81d2ed10bc1d6fbdf5c4787/Dynamic_Programming/10.RegularExpressionMatching/?at=master)

+ [27. Unique Paths](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/886569d098af51382eba7b37ffd9f5394073ff8f/Dynamic_Programming/62.UniquePaths/?at=master)

+ [28. Unique Paths II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/886569d098af51382eba7b37ffd9f5394073ff8f/Dynamic_Programming/63.UniquePathsII/?at=master)

+ [29. Minimum Path Sum](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/886569d098af51382eba7b37ffd9f5394073ff8f/Dynamic_Programming/64.MinimumPathSum/?at=master)

+ [30. Edit Distance](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/67fe3fb014be6d7f9405a6860c3d2544f58c9453/Dynamic_Programming/72.EditDistance/?at=master)

+ [31. Combinations](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/9d224d9b4e696541d16894533bcb82afbf2b4556/Dynamic_Programming/77.Combinations/?at=master)

+ [32. Subsets](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/9d224d9b4e696541d16894533bcb82afbf2b4556/Dynamic_Programming/78.Subsets/?at=master)

+ [33. Maximal Rectangle](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/95d9f851a594a7be957261019319219768d39e23/Dynamic_Programming/85.MaximalRectangle/?at=master)

+ [34. Scramble String](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/20090a879176fe6752cf168888170407c4983bed/Dynamic_Programming/87.ScrambleString/?at=master)

+ [35. Distinct Subsequences](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/bbd94ab65e7adfe9578dd86a6d5da0671461137c/Dynamic_Programming/115.DistinctSubsequences/?at=master)

+ [36. Triangle](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/dea74cff2dc29d9d75e5906d2422d7924d9cf213/Dynamic_Programming/120.Triangle/?at=master)

+ [37. Palindrome Partitioning](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/cc2e91e9ba3a1513448c143a1f87efac63c928f0/Dynamic_Programming/131.PalindromePartitioning/?at=master)

+ [38. Palindrome Partitioning II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/cc2e91e9ba3a1513448c143a1f87efac63c928f0/Dynamic_Programming/132.PalindromePartitioningII/?at=master)

****************************************************************

****************************************************************
###[Arrays]

+ [39. Two Sum](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/1.TwoSum/?at=master)

+ [40. 3 Sum Closest](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/16.3SumClosest/?at=master)

+ [41. 3 Sum](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/15.3Sum/?at=master)

+ [42. 4 Sum](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/18.4Sum/?at=master)

+ [43. Remove Duplicates from Sorted Array](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/26.RemoveDuplicatesfromSortedArray/?at=master)

+ [44. Remove Element](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/27RemoveElement/?at=master)

+ [45. Search In Rotated Sorted Array](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/33.SearchInRotatedSortedArray/?at=master)

+ [46. Search for a range](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/34.SearchforaRange/?at=master)

+ [47. Search Insert Position](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/35.SearchInsertPosition/?at=master)

+ [48. First Missing Positive](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/41.FirstMissingPositive/?at=master)

+ [49. Merge Intervals](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/56.MergeIntervals/?at=master)

+ [50. Search a 2D Matrix](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/74.Searcha2DMatrix/?at=master)

+ [51. Remove Duplicates from Sorted Array II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Arrays/80.RemoveDuplicatesfromSortedArrayII/?at=master)

+ [52. Next Permutation](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/0bcfca54c3790551d5c5b257939d710d5bbe41fd/Arrays/31.NextPermutation/?at=master)

+ [53. Combination Sum](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/279bb180f3e0866f11eb9cad642ff180d160ae1d/Arrays/39.CombinationSum/?at=master)

+ [54. Combination Sum II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/279bb180f3e0866f11eb9cad642ff180d160ae1d/Arrays/40.CombinationSumII/?at=master)

+ [55. Permutations](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/72b4a0e5050427ea5927085aae048242a03a8a77/Arrays/46.Permutations/?at=master)

+ [56. Permutations II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/72b4a0e5050427ea5927085aae048242a03a8a77/Arrays/47.PermutationsII/?at=master)

+ [57. Rotate Image](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/cd3cac71173af3353f945e9ad5a3d1f887bfa402/Arrays/48.RotateImage/?at=master)

+ [58. MaximumSubarray](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/880f22ac7391561c44056954835c60c95b06f09a/Arrays/53.MaximumSubarray/?at=master)

+ [59. Spiral Matrix](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/880f22ac7391561c44056954835c60c95b06f09a/Arrays/54.SpiralMatrix/?at=master)

+ [60. Insert Interval](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/218631067580bbe4aea89faf1e0f53cefc212fcc/Arrays/57.InsertInterval/?at=master)

+ [61. Spiral Matrix II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/218631067580bbe4aea89faf1e0f53cefc212fcc/Arrays/59.SpiralMatrixII/?at=master)

+ [62. Set Matrix Zeroes](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/b1469e044c834b6ab157b095610e9848f8da19d7/Arrays/73.SetMatrixZeroes/?at=master)

+ [63. Sort Colors](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/b1469e044c834b6ab157b095610e9848f8da19d7/Arrays/75.SortColors/?at=master)

+ [64. Minimum Window Substring](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/c3e7751996d688c156a1f15f0c3730c5a87bfa81/Arrays/76.MinimumWindowSubstring/?at=master)

+ [65. Word Search](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/cb7273c1caaf435075fba15f79ffa87adfe97a14/Arrays/79.WordSearch/?at=master)

+ [66. Search in Rotated Sorted Array II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/b5153d8276826b72ce7cbd9fe73c548f8dbd8dbe/Arrays/81.SearchinRotatedSortedArrayII/?at=master)

+ [67. Subsets II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/efe8a7cc2c93df560e20fb769b9b46bc60ce9971/Arrays/90.SubsetsII/?at=master)

+ [68. Pascal's Triangle](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/dea74cff2dc29d9d75e5906d2422d7924d9cf213/Arrays/118.Pascal'sTriangle/?at=master)

+ [69. Pascal's Triangle II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/dea74cff2dc29d9d75e5906d2422d7924d9cf213/Arrays/119.Pascal'sTriangleII/?at=master)


****************************************************************

****************************************************************
###[Linked Lists]

+ [70. Add Two Numbers](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/2.AddTwoNumbers/?at=master)

+ [71. Merge Two Sorted Lists](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/21.MergeTwoSortedLists/?at=master)

+ [72. Remove Nth Node From End of List](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/19.RemoveNthNodeFromEndofList/?at=master)

+ [73. Merge k Sorted Lists](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/23.MergekSortedLists/?at=master)

+ [74. Swap Nodes in Pairs](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/24.SwapNodesinPairs/?at=master)

+ [75. Rotate List](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/61.RotateList/?at=master)

+ [76. Remove Duplicates from Sorted List II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/82.RemoveDuplicatesfromSortedListII/?at=master)

+ [77. Partition List](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/86.PartitionList/?at=master)

+ [78. Reverse List II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/92.ReverseListII/?at=master)

+ [79. Reverse Nodes in k-Group](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/25.ReverseNodesink-Group/?at=master)

+ [80. Remove Duplicates from SortedList](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Linked_List/83.RemoveDuplicatesfromSortedList/?at=master)

+ [81. Copy List With Random Pointer](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Linked_List/138.CopyListwithRandomPointer/?at=master)

+ [82. Linked List Cycle](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Linked_List/141.LinkedListCycle/?at=master)

+ [83. Linked List Cycle II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Linked_List/142.LinkedListCycleII/?at=master)

+ [84. Reorder List](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Linked_List/143.ReorderList/?at=master)

****************************************************************

****************************************************************
###[Strings]

+ [85. Longest Substring Without Repeating Characters](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Strings/3.LongestSubstring/?at=master)

+ [86. Longest Palindromic Substring](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Strings/5.LongestPalindromicSubstring/?at=master)

+ [87. Longest Common Prefix](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Strings/14.LongestCommonPrefix/?at=master)

+ [88. Valid Parentheses](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Strings/20.ValidParentheses/?at=master)

+ [89. Count and Say](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/c61aeae585e7f7390b878cece70b5fa1c3ee336f/Strings/38.CountandSay/?at=master)

+ [90. Wildcard Matching](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/0b2797c6e9d4b90c9ffb3b0642729e4437fd57ea/Strings/44.WildcardMatching/?at=master)

+ [91. Implement strStr()](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/0b2797c6e9d4b90c9ffb3b0642729e4437fd57ea/Strings/28.ImplementstrStr()/?at=master)

+ [92. String to Integer - atoi()](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/0b2797c6e9d4b90c9ffb3b0642729e4437fd57ea/Strings/8.StringtoInteger(atoi)/?at=master)

+ [93. Group Anagrams](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/cd3cac71173af3353f945e9ad5a3d1f887bfa402/Strings/49.GroupAnagrams/?at=master)

+ [94. Length of Last Word](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/218631067580bbe4aea89faf1e0f53cefc212fcc/Strings/58.LengthofLastWord/?at=master)

+ [95. Valid Number](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/dc38285a1e2a1bfa790badf9fb2a298f63ed1a3d/Strings/65.ValidNumber/?at=master)

+ [96. Plus One](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/18e115fffe8beeda4f90042df8d1f66745b7cf05/Strings/66.PlusOne/?at=master)

+ [97. Add Binary](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/18e115fffe8beeda4f90042df8d1f66745b7cf05/Strings/67.AddBinary/?at=master)

+ [98. Text Justification](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/18e115fffe8beeda4f90042df8d1f66745b7cf05/Strings/68.TextJustification/?at=master)

+ [99. Simplify Path](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/a087a471fabd5ae98ca32a03d876f89cb00523e1/Strings/71.SimplifyPath/?at=master)

+ [100. Restore IP Addresses](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/9859bdf60e4b5248c84974d76eab42a5e862ac38/Strings/93.RestoreIPAddresses/?at=master)

+ [101. InterleavingString](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3f2824811241cb70d3bf1f4a1783806394e49672/Strings/97.InterleavingString/?at=master)

+ [102. Valid Palindrome](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/7c68fbba82118a23df33d1355b94c04f2c977a9d/Strings/125.ValidPalindrome/?at=master)

****************************************************************

****************************************************************
###[Math]

+ [103. Reverse integer (Handles overflow)](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Math/7.ReverseInteger/?at=master)

+ [104. Palindrome Number (In place and recursive)](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Math/9.PalindromeNumber/?at=master)

+ [105. Divide Integers without divsion, mode or multiplication operators](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/ac7c1955c75edb023d47a7307fbcbfe078c65199/Math/29.DivideTwoIntegers/?at=master)

+ [106. Multiply Strings](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/28b33ee09827cc388e45ec1978ff3c4b504381c3/Math/43.MultiplyStrings/?at=master)

+ [107. Pow(x, n)](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/880f22ac7391561c44056954835c60c95b06f09a/Math/50.Pow(x,%20n)/?at=master)

+ [108. Sqrt(x)](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/a087a471fabd5ae98ca32a03d876f89cb00523e1/Math/69.Sqrt(x)/?at=master)

+ [109. Gray Code](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/51c41c677f356a3ce34a4a1e50eeff80aa59155c/Math/89.GrayCode/?at=master)

+ [110. Roman to Integer](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/e16cb7134b1b9f762e064905f18e292bc9bbe448/Math/13.RomantoInteger/?at=master)

+ [111. Integer to Roman](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/e16cb7134b1b9f762e064905f18e292bc9bbe448/Math/12.IntegertoRoman/?at=master)


****************************************************************
###[Tree]

+ [112. Binary Tree Inorder Traversal](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/9859bdf60e4b5248c84974d76eab42a5e862ac38/Tree/94.BinaryTreeInorderTraversal/?at=master)

+ [113. Unique Binary Search Trees II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/9859bdf60e4b5248c84974d76eab42a5e862ac38/Tree/95.UniqueBinarySearchTreesII/?at=master)

+ [114. Unique Binary Search Trees](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/9859bdf60e4b5248c84974d76eab42a5e862ac38/Tree/96.UniqueBinarySearchTrees/?at=master)

+ [115. Validate Binary Search Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3f2824811241cb70d3bf1f4a1783806394e49672/Tree/98.ValidateBinarySearchTree/?at=master)

+ [116. Recover Binary Search Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3f2824811241cb70d3bf1f4a1783806394e49672/Tree/99.RecoverBinarySearchTree/?at=master)

+ [117. Same Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3f2824811241cb70d3bf1f4a1783806394e49672/Tree/100.SameTree/?at=master)

+ [118. Symmetric Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/6903404fb8db05f6b0c40bda5af12c7edb43381a/Tree/101.SymmetricTree/?at=master)

+ [119. Binary Tree Level Order Traversal](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/6903404fb8db05f6b0c40bda5af12c7edb43381a/Tree/102.BinaryTreeLevelOrderTraversal/?at=master)

+ [120. Binary Tree Zigzag Level Order Traversal](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/6903404fb8db05f6b0c40bda5af12c7edb43381a/Tree/103.BinaryTreeZigzagLevelOrderTraversal/?at=master)

+ [121. Maximum Depth of Binary Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/6903404fb8db05f6b0c40bda5af12c7edb43381a/Tree/104.MaximumDepthofBinaryTree/?at=master)

+ [122. Binary Tree Level Order Traversa lI](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/5586cc5a45a3a55f15be816c849b2ec55ea8b3a2/Tree/107.BinaryTreeLevelOrderTraversalII/?at=master)

+ [123. Convert Sorted Array to Binary Search Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/5586cc5a45a3a55f15be816c849b2ec55ea8b3a2/Tree/108.ConvertSortedArraytoBinarySearchTree/?at=master)

+ [124. Convert Sorted List to Binary Search Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/f5dcde740497b949a346050ea8ed556a923c90ee/Tree/109.ConvertSortedListtoBinarySearchTree/?at=master)

+ [125. Balanced Binary Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/f0c21a43521817da173136aa689b8e61a715e270/Tree/110.BalancedBinaryTree/?at=master)

+ [126. Minimum Depth of Binary Tree](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/f0c21a43521817da173136aa689b8e61a715e270/Tree/111.MinimumDepthofBinaryTree/?at=master)

+ [127. Path Sum](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/f0c21a43521817da173136aa689b8e61a715e270/Tree/112.PathSum/?at=master)

+ [128. Path Sum II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/f0c21a43521817da173136aa689b8e61a715e270/Tree/113.PathSumII/?at=master)

+ [129. Flatten Binary Tree to LinkedList](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/bbd94ab65e7adfe9578dd86a6d5da0671461137c/Tree/114.FlattenBinaryTreetoLinkedList/?at=master)

+ [130. Populating Next Right Pointers in Each Node](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/dea74cff2dc29d9d75e5906d2422d7924d9cf213/Tree/116.PopulatingNextRightPointersinEachNode/?at=master)

+ [131. Populating Next Right Pointers in Each Node II](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/dea74cff2dc29d9d75e5906d2422d7924d9cf213/Tree/117.PopulatingNextRightPointersinEachNodeII/?at=master)

+ [132. Binary Tree Maximum Path Sum](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/7c68fbba82118a23df33d1355b94c04f2c977a9d/Tree/124.BinaryTreeMaximumPathSum/?at=master)

+ [133. Binary Tree Preorder Traversal](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Tree/144.BinaryTreePreorderTraversal/?at=master)

+ [134. Binary Tree Postorder Traversal](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/Tree/145.BinaryTreePostorderTraversal/?at=master)


****************************************************************

###[Hash Table]
+ [135. Longest Consecutive Sequence](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/7d5b100d1ac9e59b37a63378a65c0ab3c7b24730/HashTable/128.LongestConsecutiveSequence/?at=master)

+ [136. LRU Cache](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/3fd161dba970123d8854a8c0cb29584d10805464/HashTable/146LRUCache/?at=master)


****************************************************************

###[Graph]
+ [137. Clone Graph](https://bitbucket.org/msreekan/algorithms-and-data-structures/src/60363925d22b2a032333fa8e9f0b08b8ea1cb4f9/Graph/133.CloneGraph/?at=master)

****************************************************************

###[*Utils*]

Generic utility routines (Binary Tree, Hash table, Stack, Queue etc) invoked by some of the above solutions.

***************************************************************